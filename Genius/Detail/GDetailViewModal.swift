//
//  GDetailViewModal.swift
//  Genius
//
//  Created by U. on 1.02.2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

class GDetailViewModal {
    
    var userMedia:GMedia?
    var selectedIndex: Int?
    var mediaCount: Int {
        guard let countNumber = userMedia?.data.count, countNumber > 0 else { return 0 }
        return countNumber
    }
    
    init(media userMedia: GMedia, selectedIndex index: Int) {
        self.userMedia = userMedia
        self.selectedIndex = index
    }
    
    func getUserImageURLString (onRow selectedIndex: Int) -> String{
        guard let igImage = self.userMedia?.data[selectedIndex].images.standardResolution.url else { return "" }
        return igImage
    }
    func getUserImageURL(onRow selectedIndex: Int) -> URL {
        guard let igImageURL = URL(string: getUserImageURLString(onRow: selectedIndex)) else { return URL(string: "")! }
        return igImageURL
        
    }
    func getLikeCount(onRow selectedIndex: Int) -> Int {
        guard let likeCount = self.userMedia?.data[selectedIndex].likes.count else { return 0}
        return likeCount
    }
    func getCommentCount(onRow selectedIndex: Int) -> Int {
        guard let commentCount = self.userMedia?.data[selectedIndex].comments.count else { return 0}
        return commentCount
    }
}
