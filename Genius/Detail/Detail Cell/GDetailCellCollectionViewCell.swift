//
//  GDetailCellCollectionViewCell.swift
//  Genius
//
//  Created by U. on 1.02.2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class GDetailCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fullScreenImage: UIImageView!
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var commentIcon: UIImageView!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.likeIcon.contentMode = UIViewContentMode.scaleAspectFit
        self.commentIcon.contentMode = UIViewContentMode.scaleAspectFit
    }

}
