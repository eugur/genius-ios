//
//  GDetailViewController.swift
//  Genius
//
//  Created by U. on 1.02.2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import Hero

class GDetailViewController: GBaseViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var onceOnly = false
    
    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    // MARK: Properties
    var detailViewModal:GDetailViewModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let nibHomeCell = UINib(nibName: "GDetailCellCollectionViewCell",
                                bundle: Bundle.main)
        detailCollectionView.register(nibHomeCell,
                                     forCellWithReuseIdentifier: "detailCell")
        self.isHeroEnabled = true
        detailCollectionView.heroModifiers = [.cascade]

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: CollectionView Delegate Methdos
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return detailViewModal?.mediaCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = detailCollectionView.dequeueReusableCell(withReuseIdentifier: "detailCell", for: indexPath) as?  GDetailCellCollectionViewCell else { return UICollectionViewCell() }
        cell.fullScreenImage.kf.setImage(with: detailViewModal?.getUserImageURL(onRow: indexPath.item), placeholder: nil, options: nil, progressBlock: { (_, _) in
            cell.fullScreenImage.windless.start()
        }) { (_, _, _, _) in
            cell.fullScreenImage.windless.end()
        }
        cell.likeCount.text = "\(detailViewModal?.getLikeCount(onRow: indexPath.item) ?? 0)"
        cell.commentCount.text = "\(detailViewModal?.getCommentCount(onRow: indexPath.item) ?? 0)"
        cell.likeIcon.image = UIImage(named: K.IMAGE_NAME.LIKE_ICON)
        cell.commentIcon.image = UIImage(named: K.IMAGE_NAME.COMMENT_ICON)
        
        cell.heroID = "perspective"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: SLDUtils.getScreenWidth(), height: SLDUtils.getScreenWidth())
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !onceOnly {
            let indexToScrollTo = IndexPath(item: detailViewModal?.selectedIndex ?? 0, section: 0)
            self.detailCollectionView.scrollToItem(at: indexToScrollTo, at: .left, animated: false)
            onceOnly = true
        }
        
    }
    
}
