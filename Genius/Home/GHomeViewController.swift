//
//  GHomeViewController.swift
//  Genius
//
//  Created by U. on 24.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import Kingfisher
import Windless
import Hero

class GHomeViewController: GBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GHomeCollectionViewCellDelegate{
    
    //MARK: Variables
    var homeViewModal: GHomeViewModal?

    //MARK: Outlets

    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    
    //MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        homeViewModal = GHomeViewModal(reloadHomeViewCallback: reloadHomeVC)
        
        let nibHomeCell = UINib(nibName: "GHomeCollectionViewCell",
                               bundle: Bundle.main)
        mediaCollectionView.register(nibHomeCell,
                                     forCellWithReuseIdentifier: "homeCell")
        
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true;
    }
    
    func reloadHomeVC ()  {
        mediaCollectionView.reloadData()
    }
    
    // MARK: CollectionView Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("media count: \(homeViewModal?.mediaCount)")
        return homeViewModal!.mediaCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCell", for: indexPath) as?  GHomeCollectionViewCell else { return UICollectionViewCell() }
        cell.homeCellDelegate = self
        cell.igPersonalImage.kf.setImage(with: homeViewModal?.getUserImageURL(onRow: indexPath.item), placeholder: nil, options: nil, progressBlock: { (_, _) in
            cell.igPersonalImage.windless.start()
        }) { (_, _, _, _) in
            cell.igPersonalImage.windless.end()
        }
        cell.igPersonalImage.heroID = "perspective"

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: SLDUtils.getScreenWidth()/3, height: SLDUtils.getScreenWidth()/3)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = GDetailViewController()
        guard let userMedia = self.homeViewModal?.userMedia else { return }
        let selectedIndex = indexPath.item
        detailVC.detailViewModal = GDetailViewModal(media: userMedia, selectedIndex: selectedIndex)
        navigationController?.isHeroEnabled = true
        navigationController?.heroNavigationAnimationType = .cover(direction: .down)
        navigationController?.pushViewController(detailVC, animated: true)
    }

}
