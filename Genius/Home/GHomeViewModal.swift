//
//  GHomeViewModal.swift
//  Genius
//
//  Created by U. on 26.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

class GHomeViewModal {
    
    var reloadViewCallback:(() -> Void)!
    var userMedia:GMedia?
    
    var mediaCount: Int {      
        guard let countNumber = userMedia?.data.count else { return 0 }
        return countNumber
    }
    
    init(reloadHomeViewCallback: @escaping(() -> Void)) {
        self.reloadViewCallback = reloadHomeViewCallback
        getMostRecentPhotos()
    }
    
    func getMostRecentPhotos() {

        
        GRouterWrapper.getMostRecentPhotos(success: { (mediaObj) in
            print("mediaObj: \(mediaObj)")
            self.userMedia = mediaObj
            if let encoded = try? JSONEncoder().encode(mediaObj) {
                UserDefaults.standard.set(encoded, forKey: "kMediaObj")
            }
            self.reloadViewCallback()
        }) { (error, statusCode) in
            if let mediaData = UserDefaults.standard.data(forKey: "kMediaObj"),
                let media = try? JSONDecoder().decode(GMedia.self, from: mediaData) {
                self.userMedia = media
                self.reloadViewCallback()
            }
            print("error: \(error) statusCode: \(String(describing: statusCode))")
        }
    }
    
    func getUserImageURLString (onRow selectedIndex: Int) -> String{
        guard let igImage = self.userMedia?.data[selectedIndex].images.lowResolution.url else { return "" }
        return igImage
    }
    func getUserImageURL(onRow selectedIndex: Int) -> URL {
        guard let igImageURL = URL(string: getUserImageURLString(onRow: selectedIndex)) else { return URL(string: "")! }
        return igImageURL
        
    }
     

}
