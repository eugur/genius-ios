//
//  GLoginViewController.swift
//  Genius
//
//  Created by U. on 20.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class GLoginViewController: GBaseViewController ,UIWebViewDelegate{
    
    @IBOutlet weak var loginWeb: UIWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    lazy var loginViewModal = GLoginViewModal()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginWeb.delegate = self
        unSignedRequest()
        
    }

    func unSignedRequest () {
        loginWeb.loadRequest(loginViewModal.loginURLRequest)
    }

    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingIndicator.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }

    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(K.INSTAGRAM_IDS.IG_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        let defaults = UserDefaults.standard
        defaults.set(authToken, forKey:K.UserDefaults.TOKEN)
        let homeVC = GHomeViewController()
        self.navigationController?.pushViewController(homeVC,
                                                      animated: true);
    }
    

}
