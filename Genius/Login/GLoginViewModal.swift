//
//  GLoginViewModal.swift
//  Genius
//
//  Created by U. on 20.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

class GLoginViewModal {
    
    var loginURL: URL {
        let urlString = "\(K.INSTAGRAM_IDS.IG_AUTHURL)?client_id=\(K.INSTAGRAM_IDS.IG_CLIENT_ID)&redirect_uri=\(K.INSTAGRAM_IDS.IG_REDIRECT_URI)&response_type=token&scope=\(K.INSTAGRAM_IDS.IG_SCOPE)&DEBUG=True"
        return URL(string: urlString)!
    }
    
    var loginURLRequest: URLRequest{
        
        let urlRequest =  URLRequest(url: loginURL)
        return urlRequest
    }
}
