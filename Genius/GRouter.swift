//
//  GRouter.swift
//  Genius
//
//  Created by U. on 22.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation
import Alamofire

enum GRouter: URLRequestConvertible {
    
    static let baseURL = K.Base.URL
    
    case getMediaRecent

    func asURLRequest() throws -> URLRequest {
        var method:HTTPMethod {
            switch self {
            case .getMediaRecent:
                return .get
//            case .upsertUser:
//                return .post
            }
        }
        
        let parameters:([String: Any]?) = {
            switch self {
            case .getMediaRecent:
                return nil
            }
        }()
        
        let url: URL = {
            let relativePath: String?
            let query: String?
            let defaults = UserDefaults.standard
            switch self {
            case .getMediaRecent:
                relativePath = "/v1/users/self/media/recent/"
                query = "access_token=\(defaults.string(forKey: K.UserDefaults.TOKEN)!)"
                
            }
            
            var urlComponents = URLComponents(string: K.Base.URL)!
            if let relativePath = relativePath {
                urlComponents.path = relativePath
            }
            
            if let query = query {
                urlComponents.query = query
            }
           
            return urlComponents.url!
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        let defaults = UserDefaults.standard
        if let token = defaults.string(forKey:K.UserDefaults.TOKEN) {
            urlRequest.addValue(token, forHTTPHeaderField: K.UserDefaults.TOKEN)
        }
        let encoding: ParameterEncoding
        
        switch self {
        case .getMediaRecent:
            encoding = URLEncoding.default
        }
        return try encoding.encode(urlRequest, with: parameters)
    }
}

class GRouterWrapper {
    
    class func getMostRecentPhotos(success:@escaping (GMedia) -> Void, failure:@escaping (Error, Int?) -> Void) {
        
        Alamofire.request(GRouter.getMediaRecent).responseData { (response) in
            switch response.result {
            case .success(let value):
                guard let responseObject = GMedia(data: value) else {return}
                success(responseObject)
            case .failure(let err):
                failure(err, response.response?.statusCode)
            }
        }
    }
}

