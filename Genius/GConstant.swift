//
//  GConstant.swift
//  Genius
//
//  Created by U. on 22.01.2018.
//  Copyright © 2018 me. All rights reserved.
//

import Foundation

struct K {
    
    struct Is {
        static let Prod: Bool = false
    }
    
    struct Base {
        static let URL = "https://api.instagram.com"
    }
    
    struct UserDefaults {
        static let TOKEN = "access_token"
    }
    
    struct INSTAGRAM_IDS {
        static let IG_AUTHURL = "https://api.instagram.com/oauth/authorize/"
        static let IG_APIURl  = "https://api.instagram.com/v1/users/"
        static let IG_CLIENT_ID  = "de335e70f6474a0484113e92bdce3d3b"
        static let IG_CLIENTSERCRET = "3d370ac776f94e5ea78d4e0f9e061d10 "
        static let IG_REDIRECT_URI = "https://www.ugureratalar.com"
        static let IG_ACCESS_TOKEN =  "access_token"
        static let IG_SCOPE = "likes+comments+relationships"
    }
    
    struct IMAGE_NAME {
        static let LIKE_ICON = "like"
        static let COMMENT_ICON = "comment"
    }
}
