//
//  SLDUtils.swift
//  AlamofireTest
//
//  Created by U on 16/10/2016.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import Alamofire

class SLDUtils: NSObject {
    
   
    static var transparentView: UIView = UIView(frame: (SLDUtils.getAppDelegate().window?.frame)!)
    
    static func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static func getJsonHeader() -> HTTPHeaders {
        
        let headers: HTTPHeaders = [
            "Cache-Control": "max-age=0",
            "Connection": "Keep-Alive",
            "Content-Type": "application/json"
        ]
        return headers
        
    }
    
    static func getFormHeader() -> HTTPHeaders {
        
        let headers: HTTPHeaders = [
            "Cache-Control": "max-age=0",
            "Connection": "Keep-Alive",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return headers
    }

    static func informWithAlertView(title: String, message: String) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                      style: UIAlertActionStyle.default,
                                      handler: nil))
        SLDUtils.getAppDelegate().window!.rootViewController!.present(alert, animated: true,
                                                                      completion: nil)
    }
    
    static func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
        
    }
    
    static func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
        
    }
    
    static func scaleImage(sourceImage: UIImage, width: CGFloat) -> UIImage {
        
        let oldWidth: CGFloat = sourceImage.size.width
        let scaleFactor: CGFloat = width / oldWidth
        
        let newHeight: CGFloat = sourceImage.size.height * scaleFactor
        let newWidth: CGFloat = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth,
                                                      height: newHeight),
                                               false,
                                               0.0)
        sourceImage.draw(in: CGRect(x: 0,
                                    y: 0,
                                    width:newWidth,
                                    height: newHeight))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
    @objc static func closeKeyboard(sender: UIBarButtonItem) {
        
        if let viewController: UIViewController = objc_getAssociatedObject(sender, "key") as? UIViewController {
            viewController.view.endEditing(true)
        }
    }
    
    static func getOkeyButton(viewController: UIViewController) -> UIToolbar {
        
        let keyboardDoneButtonToolBar: UIToolbar = UIToolbar.init()
        keyboardDoneButtonToolBar.barStyle = UIBarStyle.black
        keyboardDoneButtonToolBar.isTranslucent = true
        keyboardDoneButtonToolBar.tintColor = nil
        keyboardDoneButtonToolBar.sizeToFit()
        
        var key = "key"
        let doneButton: UIBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("OK", comment: ""),
                                                               style: UIBarButtonItemStyle.plain,
                                                               target: self,
                                                               action: #selector(closeKeyboard))
        objc_setAssociatedObject(doneButton, &key, viewController, .OBJC_ASSOCIATION_ASSIGN)
        let spacer1: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let spacer2: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        keyboardDoneButtonToolBar.setItems([spacer1, spacer2, doneButton], animated: false)
        
        return keyboardDoneButtonToolBar
        
    }
    
    static func getCalculateLabelSizeMethod(text: String, font: UIFont, sizeMake: CGSize) -> CGSize {
        
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let textRect: CGRect = text.boundingRect(with: sizeMake, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle: paragraphStyle.copy()], context: nil)
        
        return textRect.size
        
    }
    
    static func isValidEmail(validEmail: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: validEmail)
        return result
    }
    
    static func dateStringFromTimeInterval(timeInterval: TimeInterval) -> String {
        
        let date = Date(timeIntervalSince1970: timeInterval)
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let dateFormatter = DateFormatter()
        let longMonths = dateFormatter.monthSymbols
        let longMonthSymbol = longMonths?[month-1]
        return "\(longMonthSymbol!) \(year)"
        
    }
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory,
                                             in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

}


extension UIViewController: UITextFieldDelegate {
    func addToolBar(textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(red: 211.0/255.0,
                                       green: 211.0/255.0,
                                       blue: 211.0/255.0,
                                       alpha: 1.0)
        toolBar.tintColor = UIColor(red: 153/255,
                                    green: 91/255,
                                    blue: 209/255,
                                    alpha: 1)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("OK", comment: ""),
                                         style: UIBarButtonItemStyle.done,
                                         target: self,
                                         action: #selector(UIViewController.donePressed))
        let spacer1: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                                            target: nil,
                                                            action: nil)
        let spacer2: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                                            target: nil,
                                                            action: nil)
        toolBar.setItems([spacer1, spacer2, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    @objc func donePressed() {
        view.endEditing(true)
    }
    
    class AlertHelper {
        func showAlert(fromController controller: UIViewController,
                       title: String,
                       message: String) {
            let alert = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                          style: UIAlertActionStyle.default,
                                          handler: nil))
            controller.present(alert,
                               animated: true,
                               completion: nil)
        }
    }
}


